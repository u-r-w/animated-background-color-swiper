import * as React from 'react';
import { Text, View, StyleSheet, Animated } from 'react-native';
import Swiper from 'react-native-swiper';
import * as ClassIcons from './components/class-icons';

function generateHslaColors(saturation, lightness, alpha, amount) {
  let colors = [];
  let hueDelta = Math.trunc(360 / amount);

  for (let i = 0; i < amount; i++) {
    let hue = i * hueDelta;
    colors.push(`hsla(${hue}, ${saturation}%, ${lightness}%, ${alpha})`);
  }

  return colors;
}

const inputRange = Object.entries(ClassIcons).map((_, i) => i);

const FIRST_ITEM_RANGE_VALUE = inputRange[0];
const LAST_ITEM_RANGE_VALUE = inputRange[inputRange.length - 1];

const outputRange = generateHslaColors(50, 50, 1.0, inputRange.length);

outputRange.unshift(outputRange[outputRange.length - 1]);
inputRange.unshift(-1);

outputRange.push(outputRange[1]);
inputRange.push(inputRange.length - 1);

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.animatedColorValue = new Animated.Value(0);
    this.backgroundColor = this.animatedColorValue.interpolate({
      inputRange,
      outputRange,
    });
    this.lastIndex = null;
  }

  onIndexChanged = newIndex => {
    if (
      this.lastIndex === LAST_ITEM_RANGE_VALUE &&
      newIndex === FIRST_ITEM_RANGE_VALUE
    ) {
      this.animatedColorValue.setValue(inputRange[0]);
    } else if (
      this.lastIndex === FIRST_ITEM_RANGE_VALUE &&
      newIndex === LAST_ITEM_RANGE_VALUE
    ) {
      this.animatedColorValue.setValue(inputRange[outputRange.length - 1]);
    }
    Animated.timing(this.animatedColorValue, {
      toValue: newIndex,
      duration: 500,
    }).start();
    this.lastIndex = newIndex;
  };

  render() {
    const { backgroundColor, onIndexChanged } = this;
    return (
      <Animated.View style={[styles.container, { backgroundColor }]}>
        <Swiper onIndexChanged={onIndexChanged}>
          {Object.entries(ClassIcons).map(([key, LogoComponent]) => (
            <View style={styles.slide} key={key}>
              <LogoComponent height="300" width="300" />
            </View>
          ))}
        </Swiper>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 8,
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
